# o3d_1-12_bull_3_base

An extension of the 2nd base project for CMU students studying Game Engine Development.

This one combines Ogre3d (1.12) and Bullet (3.0) and demonstrates the use of a simple ray trace, this is from the centre of the cube to the floor. At each step this it output on screen providing a countdown to collision. 
